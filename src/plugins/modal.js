import Vue from 'vue';
import LayoutConfirmModal from '@/components/shared/ConfirmModal.vue';

export const Modal = {
  install(Vue, options) {
    this.EventBus = new Vue();
    Vue.component('app-modal-confirm', LayoutConfirmModal);
    Vue.prototype.$modal = {
      confirm(params) {
        Modal.EventBus.$emit('confirm', params);
      },
    };
  },
};

Vue.use(Modal);
