import { isMobile } from '@/utils/device';

export default {
  computed: {
    isMobileDevice() {
      return isMobile();
    },
  },
};
