export const isMobile = () => process.browser && window.innerWidth < 1024;

export default isMobile;
