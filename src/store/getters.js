export default {
  appVersion(state) {
    return state.version;
  },
  isSidebarExpanded(state) {
    return state.isSidebarExpanded;
  },
};
