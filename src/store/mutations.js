export default {
  updateAppVersion(state, version) {
    state.version = version;
  },
  toogleSidebar(state) {
    state.isSidebarExpanded = !state.isSidebarExpanded;
  },
};
