import Vue from 'vue';
import Vuex from 'vuex';
import persistedState from 'vuex-persistedstate';
import getters from './getters';
import actions from './actions';
import state from './state';
import mutations from './mutations';

Vue.use(Vuex);

const store = new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  plugins: [persistedState()],
  loading: false,
});

export default store;
