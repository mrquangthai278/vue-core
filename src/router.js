import Vue from 'vue';
import Router from 'vue-router';
import MainDashBoard from '@/pages/dashboard/index.vue';
import HomePage from '@/pages/dashboard/home/index.vue';
import CartPage from '@/pages/dashboard/cart/index.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: MainDashBoard,
      children: [
        {
          path: '',
          name: 'home_page',
          component: HomePage,
        },
        {
          path: '/cart',
          name: 'cart_page',
          component: CartPage,
        },
      ],
    },
  ],
});

export default router;
