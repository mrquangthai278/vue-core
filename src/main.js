import Vue from 'vue';
import 'babel-polyfill';
import App from './App.vue';
import router from './router';
import store from './store';
import '@/plugins/modal';

Vue.config.productionTip = false;
Vue.prototype.$vueEventBus = new Vue();

Vue.mixin({
  methods: {
    goToRoute: (routeName, routeParams) =>
      router.push({
        name: routeName,
        params: routeParams,
      }),
  },
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
