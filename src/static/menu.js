export const listMenuMain = [
  {
    key: 'home',
    icon: 'fa fa-home',
    title: '대시보드',
    path: 'home_page',
  },
  {
    key: 'message',
    icon: 'fa fa-commenting-o',
    title: '문의/요청',
    path: '',
  },
  {
    key: 'order',
    icon: 'fa fa-shopping-cart',
    title: '서비스 구매',
    path: 'cart_page',
  },
  {
    key: 'edit',
    icon: 'fa fa-list',
    title: '승인하기',
    path: '',
  },
  {
    key: 'edit1',
    icon: 'fa fa-list',
    title: '요청현황;',
    path: '',
  },
];

export const listMenuUser = [
  {
    key: 'home',
    icon: 'fa fa-hand-paper-o',
    title: '주문내역 조회',
    path: '',
  },
  {
    key: 'message',
    icon: 'fa fa-user',
    title: '결제내역 조회',
    path: '',
  },
  {
    key: 'order',
    icon: 'fa fa-user',
    title: '정보 수정',
    path: '',
  },
  {
    key: 'edit',
    icon: 'fa fa-sign-out',
    title: '로그아웃',
    path: '',
  },
];
